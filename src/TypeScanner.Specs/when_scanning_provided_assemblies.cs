using System;
using System.Collections.Generic;
using System.Reflection;
using Machine.Specifications;

namespace TypeScanner.Specs
{
    public class when_scanning_provided_assemblies
    {
        static ITypeScanner _messageTypeScanner;
        static List<Type> _result;
        static Assembly _theLocalAssembly;

        Establish context = () =>
        {
            _theLocalAssembly = typeof(Animal).Assembly;
            _messageTypeScanner = new TypeScanner(new List<Assembly> {_theLocalAssembly});
        };

        Because of = () => _result = _messageTypeScanner.GetTypesOf<Animal>();

        It should_only_return_those_types = () => _result.Count.ShouldEqual(6);

        It should_return_a_list_that_includes_a_chinese_panda = () => _result.ShouldContain(typeof(Panda));

        It should_return_a_list_that_includes_a_grizzly_bear = () => _result.ShouldContain(typeof(GrizzlyBear));

        It should_return_a_list_that_includes_a_kangaroo = () => _result.ShouldContain(typeof(Kangaroo));

        It should_return_a_list_that_includes_a_panda = () => _result.ShouldContain(typeof(Panda));

        It should_return_a_list_that_includes_an_african_panda = () => _result.ShouldContain(typeof(Panda));

        It should_return_a_list_that_includes_an_antelope = () => _result.ShouldContain(typeof(Antelope));
    }
}