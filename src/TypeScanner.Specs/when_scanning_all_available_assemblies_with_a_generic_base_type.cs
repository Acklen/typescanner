using System;
using System.Collections.Generic;
using Machine.Specifications;

namespace TypeScanner.Specs
{
    public class when_scanning_all_available_assemblies_with_a_generic_base_type
    {
        static ITypeScanner _typeScanner;
        static List<Type> _result;

        Establish context = () =>
        {
            _typeScanner = new TypeScanner();
        };

        Because of = () => _result = _typeScanner.GetTypesOf<Panda>();

        It should_return_a_list_that_includes_a_chinese_panda = () => _result.ShouldContain(typeof(ChinesePanda));

        It should_return_a_list_that_includes_an_african_panda = () => _result.ShouldContain(typeof(AfricanPanda));

        It should_only_return_those_types = () => _result.Count.ShouldEqual(2);
    }
}